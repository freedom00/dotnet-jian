﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person
    {
        string _name;
        //public string Name; // Name 2-100 characters long, not containing semicolons
        int _age;
        //public int Age; // Age 0-150
        string _city;
        //public string City; // City 2-100 characters long, not containing semicolons

        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new InvalidOperationException("The name must be 2-100 chars, not containing semicolons");
                }
                this._name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidOperationException("The age must be 0-150.\n\n");
                }
                this._age = value;
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new InvalidOperationException("The name must be 2-100 chars, not containing semicolons\n\n");
                }
                this._city = value;
            }
        }


        public override string ToString()
        {
            return string.Format("{0} is {1}, living in {2}",Name,Age,City);
        }
    }

    class Program
    {
        static List<Person> people = new List<Person>();
        const string filename = @"..\..\people.txt";

        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            while (true)
            {
                int choice = getMenuChoice();
                if (choice == 0)
                {
                    Console.WriteLine("Good bye!");
                    SaveAllPeopleToFile();
                    System.Environment.Exit(-1);
                    break;
                }

                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    default:
                        Console.Write("Invalied error, try again!\n\n");
                        break;
                }
            }
        }

        static int getMenuChoice()
        {
            int menu;
            try
            {
                Console.WriteLine("1. Add person info\n" +
                            "2. List persons info\n" +
                            "3. Find a person by name\n" +
                            "4. Find all persons younger than age\n" +
                            "0. Exit\n");
                //string strMenu = Console.ReadLine();
                menu = int.Parse(Console.ReadLine());
                
            }catch(FormatException ex)
            {
                menu = -1;
            }
            return menu;
        }

        static void AddPersonInfo()
        {
            Console.Write("Please enter name:");
            string name = Console.ReadLine();
            Console.Write("Please enter your age:");
            string strAge = Console.ReadLine();
            int age;
            try
            {
                age = int.Parse(strAge);
            }
            catch (FormatException ex)
            {
                Console.Write("The age invalid.\n\n");
                return;
            }

            Console.Write("Please enter city:");
            string city = Console.ReadLine();
            try
            {
                Person person = new Person (name, age, city);
                people.Add(person);
            }
            catch(InvalidOperationException ex) {
                Console.Write(ex.Message);
            }         
        }
        static void ListAllPersonsInfo()
        {
            foreach(Person p in people)
            {
                Console.WriteLine(p+"\n");
            }
        }
        static void FindPersonByName()
        {
            Console.Write("Please enter name your wnat to find:");
            string name = Console.ReadLine();
            if (name.Length < 2 || name.Length > 100 || name.Contains(";"))
            {
                Console.Write("The name must be 2-100 chars, not containing semicolons");
                return;
            }
            bool found = false;
            foreach (Person p in people)
            {                
                if(name == p.Name)
                {
                    found = true;
                    Console.WriteLine(p + "\n");                   
                }
            }
            if (!found)
            {
                Console.WriteLine("Did not find\n");
            }
        }
        static void FindPersonYoungerThan()
        {
            Console.Write("Please enter age:");
            string strAge = Console.ReadLine();

            if(!int.TryParse(strAge,out int age))
            {
                Console.Write("The age invalid.\n\n");
                return;
            }
            if (age < 0 || age > 150)
            {
                Console.Write("The age must be 0-150.\n\n");
                return;
            }
            bool found = false;
            foreach (Person p in people)
            {
                if (p.Age<=age)
                {
                    found = true;
                    Console.WriteLine(p + "\n");
                }
            }
            if (!found)
            {
                Console.WriteLine("Did not find\n");
            }
        }
        static void SaveAllPeopleToFile()
        {
            string strPeople = "";
            foreach (Person p in people)
            {
                strPeople += p.Name + ";" + p.Age + ";" + p.City +"\n";
            }
            try
            {
                File.WriteAllText(filename, strPeople);
            }catch(Exception ex)
            {
                if(ex is SystemException || ex is IOException)
                {
                    Console.Write("fsdsf");
                }else
                {
                    throw ex;
                }
            }
            
        }
        static void ReadAllPeopleFromFile()
        {
            try
            {
                string[] presonLines = File.ReadAllLines(filename);
                for (int i = 0; i < presonLines.Length; i++)
                {
                    string[] preson = presonLines[i].Split(';');
                    try
                    {
                        Person p = new Person(preson[0], int.Parse(preson[1]), preson[2]);
                        people.Add(p);
                    }
                    catch (InvalidOperationException ex)
                    {
                        Console.Write("Data line invalid:" + presonLines[i].ToString() + "-->" + ex.Message+"\n");
                    }
                    catch(FormatException ex)
                    {
                        Console.Write("Data line invalid:"+ presonLines[i].ToString()+"-->"+ex.Message + "\n");
                    }
                }
            }
            catch(FileNotFoundException ex)
            {

            }
            catch(NullReferenceException ex)
            {

            }

        }
    }
}
