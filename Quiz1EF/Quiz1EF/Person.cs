﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1EF
{
    [Table("People")]
    public class Person
    {
        public int Id{ set; get; }

        [Required]
        [StringLength(150)]
        public string Name { set; get; }

        public int Age { set; get; }

        public virtual Passport Passport { get; set; }
    }
}
