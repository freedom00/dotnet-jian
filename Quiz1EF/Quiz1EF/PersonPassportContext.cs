﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1EF
{
    public class PersonPassportContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Passport> Passports { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure Person & Passport entity
            modelBuilder.Entity<Person>()
                        .HasOptional(p => p.Passport)
                        .WithRequired(pp => pp.Person);
        }
    }
}
