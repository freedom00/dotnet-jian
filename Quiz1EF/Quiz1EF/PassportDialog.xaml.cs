﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz1EF
{
    /// <summary>
    /// Interaction logic for PassportDialog.xaml
    /// </summary>
    public partial class PassportDialog : Window
    {
        Person _person;
        public PassportDialog(Person person)
        {
            InitializeComponent();
            if (person == null)
            {
                MessageBox.Show(this, "Cannot find the person.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
                return;
            }
            lblName.Content = person.Name;
            _person = person;
            if (person.Passport != null)
            {
                tbPassportNo.Text = person.Passport.PassportNo;
                btImage.Source = ByteToImage(person.Passport.Photo);
                btAddSave.Content = "Update";
            }
            else
            {
                btAddSave.Content = "Add";
            }
        }

        private ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData == null)
                return null;
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch (NotSupportedException ex)
            {
                return null;
            }
            catch (InvalidOperationException ex)
            {
                return null;
            }
        }

        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Filter = "Image Files|*.gif;*.jpg;*.png";
            if (openFileDlg.ShowDialog() == true)
            {
                btImage.Source = new BitmapImage(new Uri(openFileDlg.FileName));
            }
        }

        private void btAddSave_Click(object sender, RoutedEventArgs e)
        {
            if (_person == null)
            {
                MessageBox.Show(this, "Cannot find the person.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                DialogResult = true;
                return;
            }
            string ppno = tbPassportNo.Text;
            Regex rgx = new Regex(@"^[A-Z]{2}[0-9]{6}$");
            if (!rgx.IsMatch(ppno))
            {
                MessageBox.Show(this, "The passport must be AB123456 format.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            byte[] bytes = null;
            var bitmapSource = (BitmapSource)btImage.Source;
            if (bitmapSource != null)
            {
                try
                {
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    using (var stream = new MemoryStream())
                    {
                        encoder.Save(stream);
                        bytes = stream.ToArray();
                    }
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "The photo cannto be saved.\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            if (_person.Passport != null)
            {
                try
                {
                    _person.Passport.PassportNo = ppno;
                    _person.Passport.Photo = bytes;
                    MainWindow.ctx.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "Updating cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                try
                {
                    Passport passport = new Passport() { Id = _person.Id, PassportNo = ppno, Photo = bytes};
                    MainWindow.ctx.Passports.Add(passport);
                    MainWindow.ctx.SaveChanges();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(this, "Adding cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
