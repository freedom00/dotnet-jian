﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarsOwnerContext ctx = new CarsOwnerContext();
        public MainWindow()
        {
            InitializeComponent();
/*          ctx.Database.Delete();
            ctx.Database.Create();*/
            getAllData();
        }
        private void getAllData()
        {
            lvDataBinding.ItemsSource = (from o in ctx.Owners.Include("CarsInGarage") select o).ToList();
        }

        public Owner selected = null;

        private void lvDataBinding_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                selected = (Owner)lvDataBinding.SelectedItem;
                tbId.Text = selected.Id + "";
                tbName.Text = selected.Name;
                btImage.Source = ByteToImage(selected.Photo);
            }
            else
            {
                selected = null;
                tbId.Text =  "";
                tbName.Text = "";
                btImage.Source = null;
            }                
        }

        private ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData == null)
                return null;
            try
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();

                ImageSource imgSrc = biImg as ImageSource;

                return imgSrc;
            }
            catch (NotSupportedException ex)
            {
                return null;
            }
            catch(InvalidOperationException ex)
            {
                return null;
            }            
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            byte[] bytes = null;
            var bitmapSource = btImage.Source as BitmapSource;
            if (bitmapSource != null)
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            try
            {
                Owner owner = new Owner() { Id = 0, Name = name, Photo = bytes};
                ctx.Owners.Add(owner);
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            getAllData();
        }

        private void btDel_Click(object sender, RoutedEventArgs e)
        {
            if(selected != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + selected, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    try
                    {
                        ctx.Owners.Remove(selected);
                        ctx.SaveChanges();
                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(this, "Deletion cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

                getAllData();
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            byte[] bytes = null;
            var bitmapSource = (BitmapSource)btImage.Source;
            if (bitmapSource != null)
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            try
            {
                selected.Name = name;
                selected.Photo = bytes;
                ctx.SaveChanges();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(this, "Adding cannot finished: \n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            getAllData();
        }

        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Filter = "Image Files|*.jpg;*.png;*.bmp;*.gif";
            if (openFileDlg.ShowDialog() == true)
            {
                btImage.Source = new BitmapImage(new Uri(openFileDlg.FileName));
            }
        }

        private void btManagerCars_Click(object sender, RoutedEventArgs e)
        {
            if (selected == null)
            {
                MessageBox.Show(this, "Please select one owner \n", "Input error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }                

            CarsManagerDialog dialog = new CarsManagerDialog(selected);
            if (dialog.ShowDialog() == true)
            {
                getAllData();
            }
        }
    }

    [Table("Owners")]
    public class Owner
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [NotMapped]
        public int carsNumber { 
            get {
                try
                {
                    return CarsInGarage.Count();
                }
                catch (ArgumentNullException ex) {
                    return 0;
                }                
            } 
        } // compute only, don't store in DB
        public byte[] Photo { get; set; }
        public ICollection<Car> CarsInGarage { get; set; } // one to many

        public override string ToString()
        {
            return string.Format("{0}-{1}", Id, Name);
        }
    }

    [Table("Cars")]
    public class Car
    {
        public int Id { get; set; }
        public string MakeModel { get; set; }
        // many to one relation
        public int OwnerId { get; set; }
        public Owner CurrentOwner { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}",Id,MakeModel);
        }
    }

    public class CarsOwnerContext : DbContext
    {
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Car> Cars { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship
            modelBuilder.Entity<Car>()
                .HasRequired<Owner>(c => c.CurrentOwner)
                .WithMany(o => o.CarsInGarage)
                .HasForeignKey<int>(c => c.OwnerId);
        }
    }

}
