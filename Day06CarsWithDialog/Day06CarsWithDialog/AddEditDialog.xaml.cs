﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Car _car;

        public Car Car
        {
            get
            {
                return _car;
            }
        }
        public bool IsEdit = false;
        public AddEditDialog(Car car)
        {
            InitializeComponent();
            comboFuleType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum)); //typeof(Car.FuelTypeEnum).GetProperties();
            comboFuleType.SelectedIndex = 0;
            if (car == null)
            {
                btAddSave.Content = "Add";
            }
            else
            {
                tfMakeModel.Text = car.MakeModel;
                sliderEngSizeL.Value = car.EngineSizeL;
                comboFuleType.Text = car.FuelType.ToString();
                btAddSave.Content = "Edit";
            }

            _car = car;
        }

        private void btAddSave_Click(object sender, RoutedEventArgs e)
        {
            string makeModel = tfMakeModel.Text;
            double engineSize = Math.Round(sliderEngSizeL.Value,1);

            if (!Enum.TryParse(comboFuleType.Text, out Car.FuelTypeEnum fuelType))
            {
                MessageBox.Show(this, "The fuel type must be selected.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(_car == null)
            {
                IsEdit = false;
                try
                {
                    Car car = new Car() { MakeModel = makeModel, EngineSizeL = engineSize, FuelType = fuelType };
                    _car = car;
                }
                catch(ArgumentException ex)
                {
                    MessageBox.Show(this, "Car can not be created:\n"+ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }
            else
            {
                try
                {
                    IsEdit = true;
                    _car.FuelType = fuelType;
                    _car.EngineSizeL = engineSize;
                    _car.MakeModel = makeModel;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, "Car can not be created:\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            DialogResult = true;
        }
    }

}
