﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07SimpleDatabase
{
    class Program
    {
        const string connetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\trakadmin\Documents\dotnet\Day07SimpleDatabase\SimpleDB.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(connetionString);
            conn.Open();
            SqlCommand command = new SqlCommand("INSERT INTO People(Name,Age) Values(@Name,@Age)", conn);
            command.Parameters.AddWithValue("@Name", "Jerry");
            command.Parameters.AddWithValue("@Age", 18);
            command.ExecuteNonQuery();

            SqlCommand comSelect = new SqlCommand("SELECT * FROM People", conn);
            SqlDataReader red = comSelect.ExecuteReader();
            while (red.Read())
            {
                int id = red.GetInt32(0);
                string name = red.GetString(1);
                int age = red.GetInt32(2);
                Console.WriteLine(id + " " + name + " " + age);
            }
            conn.Close();
            Console.ReadKey();
        }
    }
}
