﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Passenger _passenger;

        public Passenger passenger
        {
            get
            {
                return _passenger;
            }
        }
        public bool IsEdit = false;
        public AddEditDialog(Passenger pasg)
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;

            for (int i = 0; i < 24; i += 1)
            {
                for (int j = 0; j < 60; j += 30)
                    comboDeptTime.Items.Add(string.Format("{0:0#}:{1:0#}", i, j));
            }

            if (pasg == null)
            {
                btAddSave.Content = "Add";
                btDelete.IsEnabled = false;
            }
            else
            {
                tbID.Text = pasg.Id + "";
                tbName.Text = pasg.Name;
                tbPassport.Text = pasg.PassportNo;
                tbDestina.Text = pasg.Destination;
                dpDeptdate.Text = pasg.DepartureDateTime.ToString("yyyy-MM-dd");
                string dtime = pasg.DepartureDateTime.ToString("HH:mm");
/*                for (int i = 0; i < comboDeptTime.Items.Count; i++)
                {
                    string value = comboDeptTime.Items[i].ToString();
                    if(value.Replace(" ","") == dtime.Replace(" ", ""))
                    {
                        comboDeptTime.SelectedIndex = i;
                    }
                }*/
                comboDeptTime.Text = dtime;
                btAddSave.Content = "Edit";
                btDelete.IsEnabled = true;
            }

            _passenger = pasg;
        }

        private void btAddSave_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string passport = tbPassport.Text;
            string destina = tbDestina.Text;

            if (!DateTime.TryParse(dpDeptdate.Text,out DateTime deptdate))
            {
                MessageBox.Show(this, "The dept. date is invalid\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                dpDeptdate.Focus();

                return;
            }

            if (deptdate.Year<1900)
            {
                MessageBox.Show(this, "The dept. date is invalid\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                dpDeptdate.Focus();

                return;
            }

            if (!DateTime.TryParse(comboDeptTime.Text, out DateTime depttime))
            {
                MessageBox.Show(this, "The dept. time is invalid\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                comboDeptTime.Focus();
                comboDeptTime.IsDropDownOpen = true;
                return;
            }

            string deptdatetime = deptdate.ToString("yyyy-MM-dd") + " " + depttime.ToString("HH:mm");

            if (!DateTime.TryParse(deptdatetime, out DateTime deptdateDT))
            {
                MessageBox.Show(this, "The dept. date or time is invalid\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            if (passenger == null)
            {
                IsEdit = false;
                try
                {

                    Passenger pasg = new Passenger() { Id = 0, Name = name, PassportNo = passport, Destination = destina, DepartureDateTime = deptdateDT };
                    _passenger = pasg;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, "Passenger can not be created:\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                try
                {
                    IsEdit = true;

                    _passenger.Name = name;
                    _passenger.PassportNo = passport;
                    _passenger.Destination = destina;
                    _passenger.DepartureDateTime = deptdateDT;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(this, "Passenger can not be created:\n" + ex.Message, "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            DialogResult = true;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {

            if (passenger == null)
            {
                    MessageBox.Show(this, "Passenger is not existing:\n", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }
            else
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + passenger, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    IsEdit = true;

                    try
                    {
                        Globals.Db.Delete(passenger);
                    }
                    catch (DatabaseException ex)
                    {
                        MessageBox.Show(this, "Database loading data error\n" + ex.Message, "Database error");
                        return;
                    }
                }

            }
            DialogResult = true;
        }
    }

}
