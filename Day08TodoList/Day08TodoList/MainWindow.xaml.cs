﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Todo> todosList = new List<Todo>();

        public MainWindow()
        {
            InitializeComponent();

            refreshListFromDb();

            Sortby();
            lblCursorPosition.Text = "Data is ready!";
        }

        void refreshListFromDb()
        {
            try
            {
                todosList = Globals.Db.ReadAllData();
                lvDataBinding.ItemsSource = todosList;
            }
            catch (DatabaseException ex)
            {
                MessageBox.Show(this, "Database error\n" + ex.Message, "Database error");

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message);
            }
        }

        private void Sortby()
        {
            if (!radioTask.IsChecked == true)
            {
                todosList = todosList.OrderBy(a => a.Duedate).ToList();//from p in todosList orderby p.Duedate select p;                
            }                
            else
            {
                todosList = todosList.OrderBy(a => a.Task).ToList();//
            }                

            lvDataBinding.ItemsSource = todosList;

            //var selectedByLinq = from xxx in todosList where xxx.Task.Contains("d") select xxx; ;

            lvDataBinding.Items.Refresh();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog(null);
            if (dialog.ShowDialog() == true)
            {
                if(dialog.Todo != null)
                {
                    if (!dialog.IsEdit)
                    {
                        int id = 0;
                        try
                        {
                            id = Globals.Db.AddNew(dialog.Todo);
                            //MessageBox.Show(this, "New car id=" + id, "Car added");
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database loading data error\n" + ex.Message, "Database error");
                            return;
                        }                        
                    }
                    refreshListFromDb();
                    Sortby();
                    lblCursorPosition.Text = "Add is finished:" + dialog.Todo;
                }
            }
        }

        private void lvDataBinding_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddEditDialog dialog = new AddEditDialog((Todo)lvDataBinding.SelectedItem);
            if (dialog.ShowDialog() == true)
            {
                if (dialog.Todo != null)
                {
                    if (!dialog.IsEdit)
                    {
                        int id = 0;
                        try
                        {
                            id = Globals.Db.AddNew(dialog.Todo);
                            //MessageBox.Show(this, "New car id="+id, "Car added");
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database adding data error\n" + ex.Message, "Database error");
                        }
                        dialog.Todo.Id = id;
                        //todosList.Add(dialog.Todo);
                    }
                    else
                    {
                        try
                        {
                            Globals.Db.UPdateData(dialog.Todo);
                        }
                        catch (DatabaseException ex)
                        {
                            MessageBox.Show(this, "Database updating data error\n" + ex.Message, "Database error");
                        }
                    }
                    refreshListFromDb();
                    Sortby();
                    lblCursorPosition.Text = dialog.IsEdit?"Update":"Add" + " is finished:" + dialog.Todo;
                }
            }
        }

        private void displayStatus()
        {
            lblCursorPosition.Text = "You have "+ todosList.Count + " to do things";
        }

        private void miDelete_Context(object sender, RoutedEventArgs e)
        {
            if (lvDataBinding.SelectedIndex != -1)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete: \n" + lvDataBinding.SelectedItem, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    todosList.Remove((Todo)lvDataBinding.SelectedItem);
                    try
                    {
                        string str = ((Todo)lvDataBinding.SelectedItem).ToString();
                        Globals.Db.Delete((Todo)lvDataBinding.SelectedItem);
                        lblCursorPosition.Text = "Deletion is finished:" + str;
                    }
                    catch (DatabaseException ex)
                    {
                        MessageBox.Show(this, "Database deleting data error\n" + ex.Message, "Database error");
                    }
                    Sortby();                    
                }
            }
        }

        private void miFileExportTo_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                using (var writer = new StreamWriter(saveFileDialog.FileName))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(todosList);
                }
                lblCursorPosition.Text = "Export data to file is finished:";
            }
        }

        private void miFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            Sortby();
            lblCursorPosition.Text = "Data sort by "+ ((RadioButton)sender).Content +"!";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Globals.Db.CloseConnecting();
        }
    }
}
