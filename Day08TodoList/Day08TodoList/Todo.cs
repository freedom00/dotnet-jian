﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day06CarsWithDialog
{
    public class Todo
    {
        int _id;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        string _task;
        public string Task {
            set 
            {
                Regex rgx = new Regex("^.{2,15}$");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The task must be 2-15 letters.");
                }
                _task = value;
            }

            get 
            {
                return _task;
            } 
        } // 2-50 characters, no semicolons

        DateTime _duedate;
        public DateTime Duedate 
        {
            get
            {
                return _duedate;
            }
            set
            {
				if (value.Year<2010 || value.Year>2021)
                {
                    throw new ArgumentException("The duedate must be 2010-2020.");
                }
                _duedate = value;
            }
        }

        StatusEnum _statustype;
        public StatusEnum StatusType 
        {
            get
            {
                return _statustype;
            }
            set
            {
                _statustype = value;
            }
        }

        public enum StatusEnum { Done, Pending, Other }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", Task, Duedate.ToString("d"), StatusType);
        }
    }
}
