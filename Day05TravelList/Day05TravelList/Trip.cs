﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Day05TravelList
{
    public class Trip
    {

        public Trip(string destination, string name, string passport, DateTime depDate, DateTime retDate)
        {
            Destination = destination;
            Name = name;
            Passport = passport;
            setDepartureAndReturnDates(depDate, retDate);
        }

        string _destination;
        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                Regex rgx = new Regex("^[^;]{2,10}$");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The destination must be 2-10 letters, excluding semicolons.");
                }
                _destination = value;
            }
        }

        string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                Regex rgx = new Regex("^[^;]{2,5}$");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The name must be 2-5 letters, excluding semicolons.");
                }
                _name = value;
            }
        }

        string _passport;
        public string Passport
        {
            get
            {
                return _passport;
            }
            set
            {
                Regex rgx = new Regex(@"[A-Z]{2}[0-9]{6}");
                if (!rgx.IsMatch(value))
                {
                    throw new ArgumentException("The Passport must be AB123456. ");
                }
                _passport = value;
            }
        }

        DateTime _departuredate, _returndate;

        public void setDepartureAndReturnDates(DateTime depDate, DateTime retDate) 
        {
            if(depDate > retDate)
            {
                throw new ArgumentException("The return date must great then dep. date. ");
            }

            if(depDate.Year>2200 || depDate.Year<1900 || retDate.Year<1900 || retDate.Year>2200)
            {
                throw new ArgumentException("The departure and return date must between 1900-2200. ");
            }
            _departuredate = depDate;
            _returndate = retDate;
        }
        public DateTime DepartureDate
        {
            get
            {
                return _departuredate;
            }
        }
        public DateTime ReturnDate
        {
            get
            {
                return _returndate;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}({1}) travel to {2}, duration from {3} to {4}", Name, Passport, Destination, DepartureDate.ToString("yyyy-MM-dd"),ReturnDate.ToString("yyyy-MM-dd"));
        }

        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Destination, Name, Passport, DepartureDate.ToString("yyyy-MM-dd"), ReturnDate.ToString("yyyy-MM-dd"));
        }
    }

    public class SortAdorner : Adorner
    {
        private static Geometry ascGeometry =
            Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private static Geometry descGeometry =
            Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                (
                    AdornedElement.RenderSize.Width - 15,
                    (AdornedElement.RenderSize.Height - 5) / 2
                );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }
}
