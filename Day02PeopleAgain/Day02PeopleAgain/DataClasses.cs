﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    public class InvalidParameterException : Exception
    {
        public InvalidParameterException()
        {
        }

        public InvalidParameterException(string message)
            : base(message)
        {
        }

        public InvalidParameterException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public delegate void LogFailedSetterDelegate(string reason);
    
    class Person
    {
         protected Person() { }

        public static LogFailedSetterDelegate LogFailSet;

        string _name;
        public string Name
        {// 1-50 characters, no semicolons
            get
            {
                return _name;
            }
            set
            {
                Regex rgx = new Regex(@"^[A-Za-z\d\s_-]{1,10}$");
                if (rgx.IsMatch(value))
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Person failed, the name must be 1-10 characters, no semicolons:" + value);
                    }
                }
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Person failed, the name must be 1-50 characters, no semicolons:"+value);
                    }
                    throw new InvalidParameterException("The name must be 1-50 characters, no semicolons.");
                }
                _name = value;
            }
        }
        DateTime _dob;
        public DateTime Dob
        {// 0-150
            get
            {
                return _dob;
            }
            set
            {
                DateTime minDate = DateTime.Parse("1900-01-01");
                DateTime maxDate = DateTime.Parse("2020-01-01");
                int min = DateTime.Compare(value, minDate);
                int max = DateTime.Compare(value, maxDate);
                if (DateTime.Compare(value, minDate)< 0 || DateTime.Compare(value, maxDate) > 0)
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Person failed, the age must be 0-150:" + value.ToString("yyyy-MM-dd"));
                    }
                    throw new InvalidParameterException("The age must be 0-150.");
                }
                _dob = value;
            }
        }
        public Person(string dataLine)
        {
            string[] strArr = dataLine.Split(';');
            if (strArr.Length < 2)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Person failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The data line invalid: " + dataLine);
            }
            Name = strArr[0];
            try
            {
                DateTime dob = DateTime.Parse(strArr[1]);
                Dob = dob;
            }
            catch (FormatException)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Person failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The age invalid: " + dataLine);
            }            
        }
        public Person(string name, DateTime dob)
        {
            Name = name;
            Dob = dob;
        }

        public virtual string ToDataString()
        {
            return string.Format("Person;{0};{1}", Name, Dob.ToString("yyyy-MM-dd"));
        }

        public override string ToString()
        {
            return string.Format("Person name is {0}, {1} y/o", Name, Dob.ToString("yyyy-MM-dd"));
        }
    }

    class Teacher : Person
    {
        string _subject;
        public string Subject
        { // 1-50 characters, no semicolons
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Teacher failed, the subject must be 1-50 characters, no semicolons: " + value);
                    }
                    throw new InvalidParameterException("The subject must be 1-50 characters, no semicolons.");
                }
                _subject = value;
            }

        }
        int _yearsofexperience;
        public int YearsOfExperience
        {// 0-100
            get
            {
                return _yearsofexperience;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Teacher failed, the years of experience must be 0-100: " + value);
                    }
                    throw new InvalidParameterException("The years Of experience must be 0-100.");
                }
                _yearsofexperience = value;
            }
        }
        public Teacher(string dataLine) : base()
        {
            string[] strArr = dataLine.Split(';');
            if (strArr.Length < 4)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Teacher failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The data line invalid: " + dataLine);
            }
            Subject = strArr[2];

            if (!int.TryParse(strArr[3], out int yoe))
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Teacher failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The YearsOfExperience invalid: " + dataLine);
            }
            YearsOfExperience = yoe;
            base.Name = strArr[0];

            try
            {
                DateTime dob = DateTime.Parse(strArr[1]);
                Dob = dob;
            }
            catch (FormatException)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Teacher failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The dob invalid: " + dataLine);
            }
        }
        public Teacher(string name, DateTime dob, string subject, int yoe) : base(name, dob)
        {
            Subject = subject;
            YearsOfExperience = yoe;
        }
        public override string ToDataString()
        {
            return string.Format("Teacher;{0};{1};{2};{3}", base.Name, base.Dob.ToString("yyyy-MM-dd"), Subject, YearsOfExperience);
        }

        public override string ToString()
        {
            return string.Format("Teacher {0} is {1} y/o teaching {2} with {3} YearsOfExperience", base.Name, Dob.ToString("yyyy-MM-dd"), Subject, YearsOfExperience);
        }
    }

    class Student : Person
    {
        string _program;
        public string Program
        {// 1-50 characters, no semicolons
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Student failed, the Program must be 1-50 characters, no semicolons: " + value);
                    }
                    throw new InvalidParameterException("The Program must be 1-50 characters, no semicolons.");
                }
                _program = value;
            }
        }

        double _gpa;
        public double GPA
        {// 0-4.3
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0.0 || value > 4.3)
                {
                    if (LogFailSet != null)
                    {
                        LogFailSet("Create Student failed, the GPA must be 0-4.3: " + value);
                    }
                    throw new InvalidParameterException("The GPA must be 0-4.3");
                }
                _gpa = value;
            }
        }
        public Student(string dataLine) : base()
        {
            string[] strArr = dataLine.Split(';');
            if (strArr.Length < 4)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Student failed, the data line invalid: " + dataLine);
                }
                throw new InvalidParameterException("The data line invalid: " + dataLine);
            }

            base.Name = strArr[0];
            try
            {
                DateTime dob = DateTime.Parse(strArr[1]);
                Dob = dob;
            }
            catch (FormatException)
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Student failed, the dob invalid: " + dataLine);
                }
                throw new InvalidParameterException("The dob invalid: " + dataLine);
            }

            Program = strArr[2];

            if (!double.TryParse(strArr[3], out double gpa))
            {
                if (LogFailSet != null)
                {
                    LogFailSet("Create Student failed, the GPA invalid: " + dataLine);
                }
                throw new InvalidParameterException("The GPA invalid: " + dataLine);
            }
            GPA = gpa;
        }
        public Student(string name, DateTime dob, string program, double gpa) : base(name, dob)
        {
            Program = program;
            GPA = gpa;
        }

        public override string ToDataString()
        {
            return string.Format("Student;{0};{1};{2};{3}", base.Name, Dob.ToString("yyyy-MM-dd"), Program, GPA);
        }

        public override string ToString()
        {
            return string.Format("Student {0} is {1} y/o studying {2} with GPA {3}. ", base.Name, Dob.ToString("yyyy-MM-dd"), Program, GPA);
        }
    }
}
